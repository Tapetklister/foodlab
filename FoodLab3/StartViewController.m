//
//  StartViewController.m
//  FoodLab3
//
//  Created by Anton Nilsson on 2016-03-13.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import "StartViewController.h"

@interface StartViewController ()
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) BOOL blinkStatus;

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)tap:(UITapGestureRecognizer *)sender {
    
    self.animator = [[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc]initWithItems:@[self.titleLabel]];
    self.collision = [[UICollisionBehavior alloc]initWithItems:@[self.titleLabel, self.button]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    
    [self colorSchwabbleDabble];
}

-(void) colorSchwabbleDabble {
    [UIView animateWithDuration:10
                          delay: 1
                        options: kNilOptions
                     animations:^{
                         self.titleLabel.textColor = [UIColor greenColor];
                         self.titleLabel.backgroundColor = [UIColor purpleColor];
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:10
                                               delay: 1
                                             options: kNilOptions
                                          animations:^{
                                              self.titleLabel.textColor = [UIColor blueColor];
                                              self.titleLabel.backgroundColor = [UIColor yellowColor];
                                          }
                                          completion:^(BOOL finished){
                                              [self colorSchwabbleDabble];
                                          }];
                     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
