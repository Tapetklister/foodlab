//
//  FoodDetailViewController.m
//  FoodLab3
//
//  Created by Anton Nilsson on 2016-03-10.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import "FoodDetailViewController.h"

@interface FoodDetailViewController ()
@property (weak, nonatomic) IBOutlet UITableView *foodTableView;
@property (weak, nonatomic) IBOutlet UILabel *foodDetailLabel;
@property (strong, nonatomic) NSDictionary *jsonDictionary;
@property (strong, nonatomic) NSMutableData *jsonData;


@end

@implementation FoodDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.foodDetailLabel.text = self.dictionary[@"name"];
    NSLog(@"Number: %@", self.dictionary[@"number"]);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.dictionary[@"number"]]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [[NSURLConnection alloc]initWithRequest:request delegate:self];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    self.jsonData = [[NSMutableData alloc]init];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.jsonData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.jsonDictionary = [NSJSONSerialization JSONObjectWithData:self.jsonData options:kNilOptions error:nil];
    
    NSLog(@"jsonDictionary count: %d", self.jsonDictionary.count);
    [self.foodTableView reloadData];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    UIAlertView *errorView = [[UIAlertView alloc]initWithTitle:@"Fel" message:@"Kunde inte ladda data." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [errorView show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

-(int) numberOfSectionsInTableView:(UITableView *) tableView {
    return 1;
}

-(int) tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section {
    return self.jsonDictionary.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.textLabel.text = [self.jsonDictionary objectForKey:@"nutrientValues"][@"ash"];
    
    return cell;
}

/*- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.nutrientValues = [[NSMutableDictionary alloc]init];
    
    self.foodDetailLabel.text = self.dictionary[@"name"];
    
    NSString *urlString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.dictionary[@"number"]];
    NSLog(@"urlString: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"NSURL url: %@", url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error) {
            NSLog(@"Just error");
            return;
        }
        NSError *jsonParseError;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        if(jsonParseError) {
            NSLog(@"jsonParseError");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *tempNutrients = result[@"nutrientValues"];
            if(tempNutrients.count == 0) {
                NSLog(@"Nu blev det fel igen!");
            } else {
                self.nutrientValues = tempNutrients.mutableCopy;
                NSLog(@"NUTRIENT VALUES: %@", self.nutrientValues);
            }
        });
    }];
    NSLog(@"NUTRIENT VALUES: %@", self.nutrientValues);
    [task resume];
    
    [self.tableView reloadData];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
