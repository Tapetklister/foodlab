//
//  FoodDetailViewController.h
//  FoodLab3
//
//  Created by Anton Nilsson on 2016-03-10.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) NSString *detailString;
@property (strong, nonatomic) NSDictionary *dictionary;

@end
