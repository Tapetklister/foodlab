//
//  TableViewController.m
//  FoodLab3
//
//  Created by Anton Nilsson on 2016-03-10.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import "TableViewController.h"
#import "FoodDetailViewController.h"

@interface TableViewController ()

@property (strong, nonatomic) NSArray *result;
@property (strong, nonatomic) NSArray *filteredResult;

@property (strong, nonatomic) UISearchController *searchController;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.result = [[NSArray alloc]init];
    self.filteredResult = [[NSArray alloc]init];
    
    NSString *urlString = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query="];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error){
            NSLog(@"Error: %@", error);
            return;
        }
        NSError *jsonParseError;
        self.result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        NSLog(@"Result count: %lu", (unsigned long)self.result.count);
        NSLog(@"Result: %@", self.result);
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
        }
    }];
    [task resume];
    
    self.searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    [self filter];
}

-(void) updateSearchResultsForSearchController:(UISearchController *)searchController {
    [self filter];
}

//No touchy
-(void) filter {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains [c] %@", self.searchController.searchBar.text];
    self.filteredResult = [self.result filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
}

//No touchy
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


//No touchy
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == self.tableView) {
        return self.result.count;
    } else {
        [self filter];
        return self.filteredResult.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    if(self.searchController.isActive && self.searchController.searchBar.text.length>0) {
        cell.textLabel.text = self.filteredResult[indexPath.row][@"name"];
    } else {
        cell.textLabel.text = self.result[indexPath.row][@"name"];
    }
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"ShowFoodDetail"]) {
        
        NSIndexPath *indexPath = [sender indexPath];
        NSDictionary *food;
        
        if(self.searchController.isActive && self.searchController.searchBar.text.length>0) {
            food = self.filteredResult[indexPath.row];
        } else {
            food = self.result[indexPath.row];        }
        
        FoodDetailViewController *detailViewController = [segue destinationViewController];
        detailViewController.dictionary = food;
    }
    
}


@end
