//
//  AppDelegate.h
//  FoodLab3
//
//  Created by Anton Nilsson on 2016-03-10.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

